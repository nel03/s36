//1
const express = require('express');

//8
const mongoose = require('mongoose');

//2
const app = express();

const taskRoutes = require('./routes/taskRoutes.js')
//3
const port = 4001;

//7
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//9
mongoose.connect(`mongodb+srv://nel03:jonsera03@zuitt-batch197.r9sr8xv.mongodb.net/S36-Activity-2?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

//10
let db = mongoose.connection;

db.on('error', () => console.log('Connection Error'));
db.once('open', () => console.log('Connected to MongoDB!'));

//17
app.use('/tasks', taskRoutes)

//4
app.listen(port, () => console.log(`Server listening on ${port}`));