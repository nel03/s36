//11
const mongoose = require('mongoose');

//12
const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: 'Pending'
    }
});

//13
module.exports = mongoose.model('Task', taskSchema)