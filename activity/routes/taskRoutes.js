//5
const express = require('express');

//16
const TaskController = require('../controllers/TaskController.js');

//6
const router = express.Router();

//18
router.get('/:id', (req, res) => {
    TaskController.getTask(req.params.id, req.body).then((resultFromController) => res.send(resultFromController))
});

//20
router.post('/create', (req, res) => {
    TaskController.createTask(req.body).then((resultFromController) => res.send(resultFromController))
});

//22
router.put('/:id/status', (req, res) => {
    TaskController.updateStatus(req.params.id, req.body).then((resultFromController) => res.send(resultFromController))
});

module.exports = router;
