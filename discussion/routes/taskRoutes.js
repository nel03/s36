// Import express and any controllers using the 'require' directive.
// Contains all the endpoints for our application
// We separate the routes such that "app.js" only contains information on the server
// We need to use express' Router() function to achieve this
const express = require('express');
// The "taskController" allows us to use the functions defined in the "taskController.js" file
const TaskController = require('../controllers/TaskController.js')
//creates a Router instance that functions as middleware and round routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

// Routes
// The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed
// They invoke the controller functions from the controller files
// All the business logic is done in the controller

// Route for getting all tasks - runs/invokes the getAllTasks function from the controller
router.get('/', (req, res) => {
    // "resultFromController" is only used here to make the code easier to understand but it's common practice to use the shorthand parameter name for a result using the parameter name "result"/"res"
    TaskController.getAllTasks().then((resultFromController) => res.send(resultFromController));
})

// Route for creating task - runs the createTask function fro the controller
router.post('/create', (req, res) => {
	TaskController.createTask(req.body).then((resultFromController) => res.send(resultFromController))
})

router.put('/:id/update', (req, res) => {
    TaskController.updateTask(req.params.id, req.body).then((resultFromController) => res.send (resultFromController))
})

router.delete('/:id/delete', (req, res) => {
    TaskController.deleteTask(req.params.id).then((resultFromController) => res.send(resultFromController))
})


//Activity
//Get by Id
router.get('/:id', (req, res) => {
    TaskController.getTask(req.params.id).then((resultFromController) => res.send(resultFromController));
})

//Update Status
router.put('/:id/status', (req, res) => {
    TaskController.updateStatus(req.params.id, req.body).then((resultFromController) => res.send(resultFromController))
})

// Use "module.exports" to export the router object to use in the "app.js"
module.exports = router;