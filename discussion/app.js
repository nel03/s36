// Express server setup
const express = require('express');
//Mongoose
const mongoose = require('mongoose');
// This allows us to use all the routes defined in the "taskRoute.js"
const taskRoutes = require('./routes/taskRoutes.js')
const app = express();

const port = 3001;

// Middlewares:
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect(`mongodb+srv://nel03:jonsera03@zuitt-batch197.r9sr8xv.mongodb.net/S36-Activity?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection;
db.on('error', () => console.error('Connection Error'));
db.once('open', () => console.log('Connected to MongoDb!'))


//http://localhost:3001/tasks/(URI)
// Assigns an endpoint for every database collections
app.use('/tasks', taskRoutes)


app.listen(port, () => console.log(`Server is running at port ${port}`))