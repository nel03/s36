//1
const express = require('express');

//13
const dbConnect = require('./data/database.js')

//7
const taskRoutes = require('./routes/taskRoutes.js');


//2
const port = 5001;

//3
const app = express();

//5
app.use(express.json());
app.use(express.urlencoded({extended: true}));
//17
app.use('/tasks', taskRoutes);


//4
app.listen(port, () => console.log(`Server listening on ${port}`));