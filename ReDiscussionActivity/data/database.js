//8
const mongoose = require('mongoose');

//9
mongoose.connect(`mongodb+srv://nel03:jonsera03@zuitt-batch197.r9sr8xv.mongodb.net/s36c-Activity?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

//10
let dbConnect = mongoose.connection;

//11
dbConnect.on('error', () => {
    console.error('Error Connection')
});
dbConnect.once('open', () => {
    console.log('Connected to MongoDb!')
});

//12
module.exports = dbConnect