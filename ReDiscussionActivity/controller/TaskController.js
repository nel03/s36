//19
const mongoose = require('mongoose');
const Task = require('../models/Task.js');

//20
const task = require('../models/Task.js');

//24
async function createTasks(req, res){
    try{
        const findDuplicate = await Task.find({name: req.body.name})
        
        if (findDuplicate !== null && findDuplicate.length > 0){
            return res.send("Duplicate Task Found!")
        }

        const task = new Task({
            name: req.body.name
        });

        const result = await task.save();
        return res.send(result);
    } catch(error){
        console.log(error);
        return res.send(error)
    }
};

//26
async function getIdTask(req,res){
    try{
        const result = await Task.findById(req.params.id);
        res.send(result)
    }catch(error){
        return res.send(error)
    }
};

//28
async function updateTask(req,res){
    try{
        const task = await Task.findById(req.params.id);
        if (task){
            task.name = req.body.name
            const update = await task.save()
            return res.send(update);
        }else{
            return res.send('Input a Valid Id!')
        }
    }catch(error){
        console.log(error);
        return res.send(error)
    }
};

//30
async function updateStatusTask(req,res){
    try{
        const task = await Task.findById(req.params.id)
        if(task){
            task.status = req.body.status;
            const update = await task.save()
            return res.send(update)
        }else{
            return res.send('Status still Pending!')
        }
    }catch(error){
        console.log(error)
        return res.send(error)
    }
};

//32
async function deleteTask(req,res){
    try{
        const taskDelete = await Task.findByIdAndRemove(req.params.id)
        if(taskDelete){
            return res.send(taskDelete)
        }
        return res.send('Task was not deleted!')
    }catch(error){
        console.log(error)
        res.send(error)
    }
};

module.exports = {
    createTasks: createTasks,
    getIdTask: getIdTask,
    updateTask: updateTask,
    updateStatusTask: updateStatusTask,
    deleteTask:deleteTask
};


