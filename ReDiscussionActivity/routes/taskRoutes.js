//6
const express = require('express');

//21
const TaskController = require('../controller/TaskController.js');

//22
const router = express.Router();

//23
router.post('/create', TaskController.createTasks);
//25
router.get('/:id', TaskController.getIdTask)
//27
router.put('/:id/update', TaskController.updateTask);
//29
router.put('/:id/status', TaskController.updateStatusTask);
//31
router.delete('/:id/delete', TaskController.deleteTask)

module.exports = router