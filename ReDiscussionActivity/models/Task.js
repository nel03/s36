//14
const mongoose = require('mongoose');

//15
const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: 'Pending'
    }
});

//16
module.exports = mongoose.model('Task', taskSchema);